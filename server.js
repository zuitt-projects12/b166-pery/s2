const express = require("express");
const app = express();
const PORT = process.env.PORT || 5001;

app.use(express.json());

require("./app/routes")(app, {})

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
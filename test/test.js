const { factorial, oddOrEven, div_check } = require("../src/util");
const { expect, assert } = require("chai");

describe("test_fun_factorials", () => {
    it("test_fun_factorial_5!_is_120", () => {
        const product = factorial(5);
        expect(product).to.equal(120);
    });
    
    it("test_fun_factorial_1!_is_1", () => {
        assert.equal(factorial(1), 1);
    });
});

describe("test_fun_factorials_activity", () => {
    it("test_fun_factorial_0!_is_1", () => {
        assert.equal(factorial(0), 1);
    });
    it("test_fun_factorial_4!_is_24", () => {
        expect(factorial(4)).to.equal(24);
    });
    
    it("test_fun_factorial_10!_is_3628800", () => {
        assert.equal(factorial(10), 3628800);
    });

    // Test for negative numbers
    it("Test factorial -1 is undefined", () => {
        const product = factorial(-1);
        assert.equal(product, undefined);
    });

    // Test if non-numeric value returns an error
    it("Test factorial non-numeric value returns an error", () => {
        assert.equal(factorial("1"), undefined);
    });
});

describe("test_odd_or_even", () => {
    it("Test non-numeric value returns undefined", () => assert.equal(oddOrEven("1"), undefined));
    
    it("test_2_is_even", () => {
        expect(oddOrEven(2)).to.equal("Even"); // uses chainable language to construct assertions
    });

    it("test_5_is_odd", () => {
        assert.equal(oddOrEven(5), "Odd"); // uses assert-dot notation that node.js has
    });
});

describe("test_divisible_by_5_or_7", () => {
    it("Test non-numeric value returns undefined", () => assert.equal(div_check("1"), undefined));
    it("test_105_is_divisible_by_5", () => assert.equal(div_check(105), true));
    it("test_14_is_divisible_by_7", () => assert.equal(div_check(14), true));
    it("test_0_is_divisible_by_5_or_7", () => assert.equal(div_check(0), true));
    it("test_22_is_not_divisible_by_5_or_7", () => assert.equal(div_check(22), false));
});
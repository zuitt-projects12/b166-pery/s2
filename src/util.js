const factorial = (n) => {
    if(typeof n !== "number") return undefined;
    if(n < 0) return undefined;
    if(n === 0) return 1;
    if(n === 1) return 1;
    return n * factorial(n - 1);
};

const oddOrEven = (n) => {
    if(typeof n !== "number") return undefined;
    if(n % 2 === 0) return "Even";
    return "Odd";
};

const div_check = (n) => {
    if(typeof n !== "number") return undefined;
    if(n % 35 === 0 || n === 0) return true;
    if(n % 5 === 0) return true;
    if(n % 7 === 0) return true;
    return false;

};

const names = {
    "Boba": {
        "name": "Boba Fett",
        "age": 50
    },
    "Anakin": {
        "name": "Anakin Skywalker",
        "age": 65
    }
};

module.exports = {
    factorial: factorial,
    oddOrEven: oddOrEven,
    div_check,
    names
};